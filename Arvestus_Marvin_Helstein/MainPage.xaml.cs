﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Arvestus_Marvin_Helstein
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private String Username = "user";
        private String Password = "SecretPassword";
        private int Tries = 0;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            if (Tries >= 3)
            {
                TextError.Text = "Sisselogimine ebaõnnestus! Rohkem katseid sisenemiseks ei ole.";
                return;
            }
            Tries++;
            string enteredUsername, enteredPassword;
            enteredUsername = InputUsername.Text;
            enteredPassword = InputPassword.Password;

            if (enteredUsername.Equals(Username) && enteredPassword.Equals(Password))
            {
                this.Frame.Navigate(typeof(UserPage));
            }
            else
            {
                TextError.Text = "Sisselogimine ebaõnnestus!";
            }
        }
    }
}
